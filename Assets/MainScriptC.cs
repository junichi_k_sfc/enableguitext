﻿using UnityEngine;
using System.Collections;

public class MainScriptC : MonoBehaviour {

	public GUIText guiText;

	void Start(){
		// 初期表示は表示しない設定をしておく
		guiText.enabled = false;
	}

	// 3秒経ったら「+3」を消す処理
	IEnumerator enabledText(){
		// 表示する
		guiText.enabled = true;

		// 3秒待つ
		yield return new WaitForSeconds(3);

		// 消す
		guiText.enabled = false;

		print("3秒経って消えました");
	}

	void Update(){
		// 判定として左クリック判定をこの場合利用しています
		if(Input.GetMouseButtonDown(0)){
			print("判定あり");
			StartCoroutine(enabledText());
			/**
			 * 注意点として、コルーチン処理は非同期処理になるため
			 * 例えば、処理的に3秒経ったら消える処理が来ると思いますが、
			 * 先にこの下に書いた「後から表示されると思うじゃん？」が先に呼ばれます
			 */
			print("後から表示されると思うじゃん？");
		}
	}
}
